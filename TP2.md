## Rapport TP Azure CLI - Création d'une image custom

**I. Création d'une image custom**

**1. Créer une VM**

**Commande:**

```bash
az vm create \
    --resource-group <RESOURCE_GROUP> \
    --name <VM_NAME> \
    --image UbuntuLTS \
    --admin-username <USERNAME> \
    --ssh-key-values <PUBLIC_KEY_PATH>
```

**2. Configurer la VM**

* Installation de Docker
* Configuration du serveur SSH
* Généralisation de la VM

**Commandes:**

```bash
# Installation de Docker
curl -fsSL https://get.docker.com | sh

# Configuration du serveur SSH
(see instructions)

# Généralisation de la VM
sudo cloud-init clean --logs --seed
sudo rm -rf /var/lib/cloud/
sudo systemctl stop walinuxagent.service
sudo rm -rf /var/lib/waagent/
sudo rm -f /var/log/waagent.log
sudo waagent -force -deprovision+user
sudo rm -f ~/.bash_history

# Suppression des ressources attribuées à la VM
az vm deallocate --resource-group <RESOURCE_GROUP> --name <VM_NAME> 

# Transformer la VM en une VM "généralisée"
az vm generalize --resource-group <RESOURCE_GROUP> --name <VM_NAME> 
```

**3. Créer une image custom**

**Commandes:**

```bash
# Créer une galerie d'images
az sig create \
   --resource-group <RESOURCE_GROUP> \
   --gallery-name <GALLERY_NAME>

# Créer une définition d'image
az sig image-definition create \
   --resource-group <RESOURCE_GROUP> \
   --gallery-name <GALLERY_NAME> \
   --gallery-image-definition <IMAGE_DEFINITION> \
   --publisher <PUBLISHER> \
   --offer <OFFER> \
   --sku <SKU> \
   --os-type Linux \
   --os-state Generalized \
   --hyper-v-generation V2 \
   --features SecurityType=TrustedLaunch

# Publier une version de l'image
az sig image-version create \
   --resource-group <RESOURCE_GROUP> \
   --gallery-name <GALLERY_NAME> \
   --gallery-image-definition <IMAGE_DEFINITION> \
   --gallery-image-version 1.0.0 \
   --target-regions "francecentral" \
   --replica-count 1 \
   --managed-image "/subscriptions/<ID>/resourceGroups/<RESOURCE_GROUP>/providers/Microsoft.Compute/virtualMachines/<VM_NAME>"
```

**4. Créer une nouvelle VM**

**Commande:**

```bash
az vm create \
    --resource-group <RESOURCE_GROUP> \
    --name <VM_NAME> \
    --image "/subscriptions/<ID>/resourceGroups/<RESOURCE_GROUP>/providers/Microsoft.Compute/galleries/<GALLERY_NAME>/images/<IMAGE_DEFINITION>/versions/1.0.0" \
    --security-type TrustedLaunch \
    --ssh-key-values <PUBLIC_KEY_PATH> --admin-username <USERNAME>
```

**II. Tester : spécialiser les VMs au lancement**

**1. Créer un fichier cloud-init.txt**

* Configuration de l'utilisateur
* Lancement d'un conteneur NGINX

**Exemple de fichier:**

```
#cloud-config

users:
  - name: <USERNAME>
    ssh_authorized_keys:
      - <PUBLIC_KEY>

packages:
  - docker

runcmd:
  - systemctl start docker
  - docker run -d -p 80:80 nginx
```

**2. Créer une nouvelle VM avec le fichier cloud-init.txt**

**Commande:**

```bash
az vm create \
    --resource-group <RESOURCE_GROUP> \
    --name <VM_NAME> \
    --image "/subscriptions/<ID>/resourceGroups/<RESOURCE_GROUP>/providers/Microsoft.Compute/galleries/<GALLERY_NAME>/images/<IMAGE_DEFINITION>/versions/1.0.0" \
    --security-type TrustedLaunch \
    --ssh-key-values <PUBLIC_KEY_PATH> --admin-username <USERNAME> \
    --custom-data cloud-init.txt
```

**3. Vérifier le fonctionnement du serveur web**

* Ouvrir le port 80 dans le firewall Azure
* Accéder à l'adresse