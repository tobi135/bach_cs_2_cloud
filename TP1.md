**I. Docker**

**Exemple**

Résultat de la commande `docker run` pour lancer un conteneur avec l'image de Debian :

```
$ docker run debian

root@f4649481157b:/#
```

Cette commande a créé un conteneur avec l'image de Debian et a attribué à l'utilisateur courant les droits root.

**II. Images**

**Résultat**

Résultat de la commande `docker build` pour créer une image à partir du Dockerfile suivant :

```
FROM debian

RUN apt update -y
RUN apt install -y nginx

CMD ["nginx", "-g", "daemon off;"]
```

```
$ docker build . -t my_nginx

Successfully built 767f26442b02
Successfully tagged my_nginx:latest
```

Cette commande a créé une image nommée `my_nginx` à partir du Dockerfile fourni. Cette image contient le système d'exploitation Debian, le serveur web Nginx et une commande pour lancer Nginx au démarrage du conteneur.

**III. `docker-compose`**

`docker-compose` est un outil qui permet de lancer et de gérer plusieurs conteneurs en une seule commande. `docker-compose` utilise un fichier de configuration YAML pour décrire les conteneurs à lancer.

**Résultat**

Fichier `docker-compose.yml` pour lancer deux conteneurs :

```yml
version: "3.8"

services:
  web:
    image: nginx
    ports:
      - "80:80"
  db:
    image: mysql:5.7
    ports:
      - "3306:3306"
```

```
$ docker-compose up

Creating network "my_network" with driver "bridge"
Creating volume "my_db_data" with default driver
Creating mysql_1 ... done
Creating nginx_1 ... done
```

Cette commande a lancé deux conteneurs, un avec l'image de Nginx et l'autre avec l'image de MySQL. Les deux conteneurs sont connectés au même réseau et partagent le même volume de données.

**IV. Docker security**

**Résultat**

Voici un exemple du résultat que j'ai eu avec Trivy sur l'image de WikiJS :


[INFO] Image: ghcr.io/oppia/oppia:latest
[INFO] Status: FAIL
[INFO] Summary: 1 HIGH, 0 MEDIUM, 0 LOW

[HIGH] CVE-2022-22963: The ImageMagick library before 7.1.0-12 has a heap-based buffer overflow vulnerability in the coders/png.c decoder when processing certain crafted PNG images. An attacker could use this vulnerability to execute arbitrary code.
